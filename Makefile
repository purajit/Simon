install:
	yarn

run:
	yarn dev

start:
	node_modules/.bin/avris-daemonise start webserver yarn dev

stop:
	node_modules/.bin/avris-daemonise stop webserver

deploy: install
	yarn generate
	echo "\nimportScripts('https://arc.io/arc-sw-core.js');" >> static/sw.js
